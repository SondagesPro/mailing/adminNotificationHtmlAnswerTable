<?php
/**
 * adminNotificationHtmlAnswerTable plugin for LimeSurvey
 * {ANSWERTABLE} with filtered HTML usage (for question text)
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2018-2022 Denis Chenu <http://sondages.pro>
 * @license AGPL v3
 * @version 2.2.0
 */
class adminNotificationHtmlAnswerTable extends PluginBase {

    private $version = "1.0";
    protected $storage = 'DbStorage';

    static protected $name = 'adminNotificationHtmlAnswerTable';
    static protected $description = '{ANSWERTABLE} with filtered HTML usage (for question text)';

    public $settings = array(
        'encodeAnswer' => array(
            'type' => 'boolean',
            'label' => "Encode answer (always filtered)",
            "default" => false,
        ),
        'showUnrelevantQuestion' => array(
            'type' => 'boolean',
            'label' => "Show unrelevant question",
            "default" => false,
        ),
    );
    public function init() {
        $this->subscribe('afterSurveyComplete');
        if(version_compare(Yii::app()->getConfig('versionnumber'),"3.14.1",">")) {
            $this->subscribe('afterFindSurvey');
            $this->subscribe('beforeControllerAction');
        }
        if(version_compare(Yii::app()->getConfig('versionnumber'),"3.14.1","<=")) {
            $this->subscribe('beforeControllerAction');
        }
    }

    public function beforeControllerAction() {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $controller = $this->getEvent()->get('controller');
        $action = $this->getEvent()->get('action');
        $subaction = $this->getEvent()->get('subaction');
        $sid = Yii::app()->getRequest()->getParam('surveyid');
        if(empty($sid) && Yii::app()->getRequest()->getIsPostRequest()) {
            $sid = Yii::app()->getRequest()->getPost('sid');
        }
        if(empty($sid)) {
            return;
        }
        /* version up to 3.14.1 : can use real emailnotificationto, delete current plugin settings and reset emailnotificationto */
        if(version_compare(Yii::app()->getConfig('versionnumber'),"3.14.1",">") ) {
            if( $this->get('emailhacked','Survey',$sid)) { /* old settings active, disable */
                $this->unsubscribe('afterFindSurvey');
                $oSurvey = Survey::model()->findByPk($sid);
                $oSurvey->emailnotificationto = $this->get('emailnotificationto','Survey',$sid,'');
                $oSurvey->emailresponseto = $this->get('emailresponseto','Survey',$sid,'');
                PluginSetting::model()->deleteAll("plugin_id = :plugin_id and model = :model and survey = :sid",array(":plugin_id"=>$this->id,":model"=>'Survey',":sid"=>$sid));
            }
            return;
        }

        /* version lesser to 3.14.1 : can't use real emailnotificationto, then hack it */
        $dataBaseUpdate = ($controller == 'admin' && $action == 'database' && Yii::app()->getRequest()->getIsPostRequest()); // This is when saving notification too
        if(!$dataBaseUpdate && empty($this->get('emailhacked','Survey',$sid)) ) {
            $oSurvey = Survey::model()->findByPk($sid);
            if(!empty($oSurvey->emailnotificationto)) {
                $this->set('emailnotificationto',$oSurvey->emailnotificationto,'Survey',$sid);
            }
            $emailnotificationto = $this->get('emailnotificationto','Survey',$sid,'');
            if(!empty($oSurvey->emailresponseto)) {
                $this->set('emailresponseto',$oSurvey->emailresponseto,'Survey',$sid);
            }
            $emailresponseto = $this->get('emailresponseto','Survey',$sid,'');
            $this->set('emailhacked',1,'Survey',$sid);
            $oSurvey->emailnotificationto = "";
            $oSurvey->emailresponseto = "";
            $oSurvey->save();
        }
        /* When saving reponseto */
        if($dataBaseUpdate) {
            if(Permission::model()->hasSurveyPermission($sid, 'surveysettings', 'update') ) {
                if(Yii::app()->getRequest()->getPost('emailnotificationto') !== null) {
                    $this->set('emailnotificationto',Yii::app()->getRequest()->getPost('emailnotificationto'),'Survey',$sid);
                    /* Bad hack … */
                    $_POST['emailnotificationto'] = '';
                }
                if(Yii::app()->getRequest()->getPost('emailresponseto') !== null) {
                    $this->set('emailresponseto',Yii::app()->getRequest()->getPost('emailresponseto'),'Survey',$sid);
                    /* Bad hack … */
                    $_POST['emailresponseto'] = '';
                }
                $this->set('emailhacked',1,'Survey',$sid);
            }
        }
        /* update the input part */
        if($controller == 'admin' && $action == 'survey' ) {
            $emailnotificationto = $this->get('emailnotificationto','Survey',$sid,'');
            $emailresponseto = $this->get('emailresponseto','Survey',$sid,'');
            $stringInfo = "<a rel=\"tooltip\" title=\"for adminNotificationHtmlAnswerTable\"> * </a> ";
            Yii::app()->getClientScript()->registerScript('emailnotificationto',"$('#emailnotificationto').val('".$emailnotificationto."');",LSYii_ClientScript::POS_POSTSCRIPT);
            Yii::app()->getClientScript()->registerScript('emailnotificationtoLabel',"$('label[for=\"emailnotificationto\"]').html('{$stringInfo}'+$('label[for=\"emailnotificationto\"]').text());",LSYii_ClientScript::POS_POSTSCRIPT);
            Yii::app()->getClientScript()->registerScript('emailresponseto',"$('#emailresponseto').val('".$emailresponseto."');",LSYii_ClientScript::POS_POSTSCRIPT);
            Yii::app()->getClientScript()->registerScript('emailresponsetoLabel',"$('label[for=\"emailresponseto\"]').html('{$stringInfo}'+$('label[for=\"emailresponseto\"]').text());",LSYii_ClientScript::POS_POSTSCRIPT);
        }
    }
    public function afterSurveyComplete() {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $afterSurveyCompleteEvent = $this->getEvent();
        $this->unsubscribe('afterFindSurvey');
        $surveyId = $afterSurveyCompleteEvent->get('surveyId');
        $oSurvey = Survey::model()->find("sid = :sid",array(":sid"=>$surveyId)); // Don't use findByPk since it's cached
        $emailnotificationto = $emailresponseto = null;
        if(version_compare(Yii::app()->getConfig('versionnumber'),"3.14.1",">")) {
            $emailnotificationto = $oSurvey->emailnotificationto;
            $emailresponseto = $oSurvey->emailresponseto;
        }
        if(version_compare(Yii::app()->getConfig('versionnumber'),"4.0.0",">")) {
            $aSurveyOptions = $oSurvey->aOptions;
            if (!empty($aSurveyOptions['emailnotificationto'])) {
                $emailnotificationto = $aSurveyOptions['emailnotificationto'];
            }
            if (!empty($aSurveyOptions['emailresponseto'])) {
                $emailresponseto = $aSurveyOptions['emailresponseto'];
            }
        }
        /* Always get current value (if user don't save survey after update) */
        if(empty($emailnotificationto)) {
            $emailnotificationto = $this->get('emailnotificationto','Survey',$surveyId,'');
        }
        if(empty($emailresponseto)) {
            $emailresponseto = $this->get('emailresponseto','Survey',$surveyId,'');
        }
        if(empty($emailnotificationto) && empty($emailresponseto) ) {
            return;
        }
        $responseId = $afterSurveyCompleteEvent->get('responseId');
        if(empty($responseId)) {
            return;
        }
        $this->ownSendNotificationEmail($afterSurveyCompleteEvent, $surveyId, $responseId, $emailnotificationto, $emailresponseto);

    }

    /**
     * Send the final email
     * @param \Event
     * @param integer $responseId
     * @param string $emailnotificationto
     * @param string $emailresponseto
     * @return void
     */
    private function ownSendNotificationEmail($afterSurveyCompleteEvent, int $surveyId, int $responseId, string $emailnotificationto, string $emailresponseto)
    {
        if(version_compare(Yii::app()->getConfig('versionnumber'),"4.0.0","<")) {
            return $this->ownSendNotificationEmail3LTS($afterSurveyCompleteEvent, $surveyId, $responseId, $emailnotificationto, $emailresponseto);
        }
        $debug = App()->getConfig('debug') || Permission::model()->hasSurveyPermission($surveyId, 'surveysettings', 'update');

        $language = Yii::app()->getLanguage();
        $oSurvey = Survey::model()->findByPk($surveyId);
        $thissurvey = $aSurveyInfo = getSurveyInfo($surveyId,Yii::app()->getLanguage());
        $sitename = Yii::app()->getConfig("sitename");
        $bIsHTML = $oSurvey->getIsHtmlEmail();

        $mailer = \LimeMailer::getInstance(\LimeMailer::ResetComplete);
        $mailer->setSurvey($surveyId);
        $mailer->aUrlsPlaceholders = ['VIEWRESPONSE','EDITRESPONSE','STATISTICS'];

        $aReplacementVars = array();
        $aReplacementVars['STATISTICSURL'] = App()->getController()->createAbsoluteUrl("/admin/statistics/sa/index/surveyid/{$surveyId}");
        $aReplacementVars['ANSWERTABLE'] = $this->ownGetAnswerTable($surveyId, $responseId, $language, $bIsHTML);
        $aReplacementVars['EDITRESPONSEURL'] = App()->getController()->createAbsoluteUrl("/admin/dataentry/sa/editdata/subaction/edit/surveyid/{$surveyId}/id/{$responseId}");
        $aReplacementVars['VIEWRESPONSEURL'] = App()->getController()->createAbsoluteUrl("responses/view/", ['surveyId' => $surveyId, 'id' => $responseId]);
        $aEmailResponseTo = array();
        $aEmailNotificationTo = array();
        $sResponseData = "";
        if (!empty($emailnotificationto)) {
            $aRecipient = explode(";", ReplaceFields($emailnotificationto, array('{ADMINEMAIL}' =>$oSurvey->adminemail), true));
            foreach ($aRecipient as $sRecipient) {
                $sRecipient = trim($sRecipient);
                if ($mailer::validateAddress($sRecipient)) {
                    $aEmailNotificationTo[] = $sRecipient;
                }
            }
        }
        if (!empty($emailresponseto)) {
            $aRecipient = explode(";", ReplaceFields($emailresponseto, array('{ADMINEMAIL}' =>$oSurvey->adminemail), true));
            foreach ($aRecipient as $sRecipient) {
                $sRecipient = trim($sRecipient);
                if ($mailer::validateAddress($sRecipient)) {
                    $aEmailResponseTo[] = $sRecipient;
                }
            }
        }
        $reData = array('thissurvey' => $thissurvey);
        templatereplace(
            "{SID}",
            array(), /* No tempvars update (except old Replacement like */
            $reData /* Be sure to use current survey */
        );
        LimeExpressionManager::updateReplacementFields($aReplacementVars);

        if (count($aEmailNotificationTo) > 0) {
            $mailer = LimeMailer::getInstance();
            $mailer->setTypeWithRaw('admin_notification', $language);
            foreach ($aEmailNotificationTo as $sRecipient) {
                $mailer->setTo($sRecipient);
                if (!$mailer->SendMessage() && $debug) {
                    $afterSurveyCompleteEvent->getContent($this)->addContent(
                        sprintf($this->gT("Notification email could not be sent to %; Reason: %s"), CHtml::encode($sRecipient), CHtml::encode($mailer->$getError))
                    );
                }
            }
        }
        if (count($aEmailResponseTo) > 0) {
            $mailer = LimeMailer::getInstance();
            $mailer->setTypeWithRaw('admin_responses', $language);
            foreach ($aEmailResponseTo as $sRecipient) {
                $mailer->setTo($sRecipient);
              if (!$mailer->SendMessage() && $debug) {
                    $afterSurveyCompleteEvent->getContent($this)->addContent(
                        sprintf($this->gT("Response email could not be sent to %; Reason: %s"), CHtml::encode($sRecipient), CHtml::encode($mailer->$getError))
                    );
                }
            }
        }
    }

    /**
     * Send the final email for old 3LTS
     * @param \Event
     * @param integer $responseId
     * @param string $emailnotificationto
     * @param string $emailresponseto
     * @return void
     */
    private function ownSendNotificationEmail3LTS($afterSurveyCompleteEvent, $surveyId, $responseId, $emailnotificationto, $emailresponseto)
    {
        global $maildebug;
        $debug = Yii::app()->getConfig('debug');
        // Usage of templatereplace
        $language = Yii::app()->getLanguage();
        $oSurvey = Survey::model()->findByPk($surveyId);
        $aSurveyInfo = getSurveyInfo($surveyId,App()->getLanguage());
        $sitename = Yii::app()->getConfig("sitename");
        $bIsHTML = ($oSurvey->htmlemail == 'Y');
        $aReplacementVars = array();
        $aReplacementVars['ADMINNAME'] = $oSurvey->admin;
        $aReplacementVars['ADMINEMAIL'] = $oSurvey->adminemail;
        $aReplacementVars['VIEWRESPONSEURL'] = Yii::app()->getController()->createAbsoluteUrl("/admin/responses/sa/view/",array('surveyid'=>$surveyId,'id'=>$responseId));
        $aReplacementVars['EDITRESPONSEURL'] = Yii::app()->getController()->createAbsoluteUrl("/admin/dataentry/sa/editdata/subaction/edit",array('surveyid'=>$surveyId,'id'=>$responseId));
        $aReplacementVars['STATISTICSURL'] = Yii::app()->getController()->createAbsoluteUrl("/admin/statistics/sa/index/surveyid",array('surveyid'=>$surveyId));
        if ($bIsHTML) {
            $aReplacementVars['VIEWRESPONSEURL'] = "<a href='{$aReplacementVars['VIEWRESPONSEURL']}'>{$aReplacementVars['VIEWRESPONSEURL']}</a>";
            $aReplacementVars['EDITRESPONSEURL'] = "<a href='{$aReplacementVars['EDITRESPONSEURL']}'>{$aReplacementVars['EDITRESPONSEURL']}</a>";
            $aReplacementVars['STATISTICSURL'] = "<a href='{$aReplacementVars['STATISTICSURL']}'>{$aReplacementVars['STATISTICSURL']}</a>";
        }
        $aReplacementVars['ANSWERTABLE'] = $this->ownGetAnswerTable($surveyId,$responseId,$language,$bIsHTML);
        $aEmailResponseTo = array();
        $aEmailNotificationTo = array();
        $sResponseData = "";
        if (!empty($emailnotificationto)) {
            $aRecipient = explode(";", ReplaceFields($emailnotificationto, array('{ADMINEMAIL}' =>$oSurvey->adminemail), true));
            foreach ($aRecipient as $sRecipient) {
                $sRecipient = trim($sRecipient);
                if (validateEmailAddress($sRecipient)) {
                    $aEmailNotificationTo[] = $sRecipient;
                }
            }
        }
        if (!empty($emailresponseto)) {
            $aRecipient = explode(";", ReplaceFields($emailresponseto, array('{ADMINEMAIL}' =>$oSurvey->adminemail), true));
            foreach ($aRecipient as $sRecipient) {
                $sRecipient = trim($sRecipient);
                if (validateEmailAddress($sRecipient)) {
                    $aEmailResponseTo[] = $sRecipient;
                }
            }
        }
        $reData = array(
            'thissurvey' => $aSurveyInfo,
        );
        $sFrom = $oSurvey->admin.' <'.$oSurvey->adminemail.'>';

        $aAttachments = unserialize($aSurveyInfo['attachments']);
        if (count($aEmailNotificationTo) > 0) {
            $aRelevantAttachments = array();
            if (isset($aAttachments['admin_notification'])) {
                foreach ($aAttachments['admin_notification'] as $aAttachment) {
                    $relevance = $aAttachment['relevance'];
                    // If the attachment is relevant it will be added to the mail.
                    if (LimeExpressionManager::ProcessRelevance($relevance) && file_exists($aAttachment['url'])) {
                        $aRelevantAttachments[] = $aAttachment['url'];
                    }
                }
            }
            // NOTE: those occurences of template replace should stay here. User from backend could use old replacement keyword
            $sMessage = templatereplace($aSurveyInfo['email_admin_notification'], $aReplacementVars, $reData, 'admin_notification', $oSurvey->anonymized == "Y", null, array(), true);
            $sSubject = templatereplace($aSurveyInfo['email_admin_notification_subj'], $aReplacementVars, $reData, 'admin_notification_subj', ($aSurveyInfo['anonymized'] == "Y"), null, array(), true);

            foreach ($aEmailNotificationTo as $sRecipient) {
                if (!SendEmailMessage($sMessage, $sSubject, $sRecipient, $sFrom, $sitename, $bIsHTML, getBounceEmail($surveyId), $aRelevantAttachments)) {
                    if ($debug > 0) {
                        $afterSurveyCompleteEvent->getContent($this)->addContent("Email could not be sent. Reason: ".$maildebug);
                    }
                }
            }
        }
        if (count($aEmailResponseTo) > 0) {
            $aRelevantAttachments = array();
            if (isset($aAttachments['detailed_admin_notification'])) {
                foreach ($aAttachments['detailed_admin_notification'] as $aAttachment) {
                    $relevance = $aAttachment['relevance'];
                    // If the attachment is relevant it will be added to the mail.
                    if (LimeExpressionManager::ProcessRelevance($relevance) && file_exists($aAttachment['url'])) {
                        $aRelevantAttachments[] = $aAttachment['url'];
                    }
                }
            }
            // NOTE: those occurences of template replace should stay here. User from backend could use old replacement keyword
            $sMessage = templatereplace($aSurveyInfo['email_admin_responses'], $aReplacementVars, $reData, 'admin_notification', $oSurvey->anonymized == "Y", null, array(), true);
            $sSubject = templatereplace($aSurveyInfo['email_admin_responses_subj'], $aReplacementVars, $reData, 'admin_notification_subj', ($aSurveyInfo['anonymized'] == "Y"), null, array(), true);
            foreach ($aEmailResponseTo as $sRecipient) {
                if (!SendEmailMessage($sMessage, $sSubject, $sRecipient, $sFrom, $sitename, $bIsHTML, getBounceEmail($surveyId), $aRelevantAttachments)) {
                    if ($debug > 0) {
                        $afterSurveyCompleteEvent->getContent($this)->addContent("Email could not be sent. Reason: ".$maildebug);
                    }
                }
            }
        }
    }

    /**
     * see eventt
     */
    public function afterFindSurvey() {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $controller = Yii::app()->getController();
        if(!empty($controller->getId()) && $controller->getId()=="survey") {
            $surveyId = $this->getEvent()->get('surveyid');
            $this->getEvent()->set('emailnotificationto','');
            $this->getEvent()->set('emailresponseto','');
        }
    }

    /**
     * Get the answer table with HTMLM filtred only
     * @param integer $surveyid
     * @param integer $srid
     * @param string $language
     * @param boolean $bIsHTML
     * @return string HTML for answer table
     */
    private function ownGetAnswerTable($surveyid,$srid,$language,$bIsHTML) {
        $showUnrelevantQuestion = $this->get('showUnrelevantQuestion',"Survey",$surveyid,$this->get('showUnrelevantQuestion',null,null,$this->settings['showUnrelevantQuestion']['default']));
        $aFullResponseTable = getFullResponseTable($surveyid, $srid, $language,!$showUnrelevantQuestion);
        $ResultTableHTML = "<table class='printouttable' >\n";
        $ResultTableText = "\n\n";
        $oldgid = 0;
        $oldqid = 0;
        Yii::import('application.helpers.viewHelper');
        $encodeAnswer = $this->get('encodeAnswer',"Survey",$surveyid,$this->get('encodeAnswer',null,null,$this->settings['encodeAnswer']['default']));
        /* Get DB columns data : string(5) mean answer */
        $tableSchemaColumns = SurveyDynamic::model($surveyid)->getTableSchema()->columns;
        foreach ($aFullResponseTable as $sFieldname=>$fname) {
            $questionHtml=viewHelper::purified($fname[0]);
            $questionText=viewHelper::flatEllipsizeText($fname[0],true,0);
            $subQuestionHtml=viewHelper::purified($fname[1]);
            $subQuestionText=viewHelper::flatEllipsizeText($fname[1],true,0);
            $answer = isset($fname[2]) ? $fname[2] : '';
            if (substr($sFieldname,0,4)=='gid_') {
                $ResultTableHTML .= "\t<tr class='printanswersgroup'><td colspan='2'>{$questionHtml}</td></tr>\n";
                $ResultTableText .="\n** {$questionText} ** \n";
            } elseif (substr($sFieldname,0,4)=='qid_') {
                $ResultTableHTML .= "\t<tr class='printanswersquestionhead'><td  colspan='2'>{$questionHtml}</td></tr>\n";
                $ResultTableText .="* {$questionText} \n";
            } else {
                if($showUnrelevantQuestion || LimeExpressionManager::ProcessStepString("{".$sFieldname.".relevanceStatus}") ) {
                    if(isset($tableSchemaColumns[$sFieldname]->dbType) && in_array($tableSchemaColumns[$sFieldname]->dbType,array('text')) ) {
                        $answer = CHtml::encode($answer);
                    } else {
                        if( $encodeAnswer ) {
                            $answer = CHtml::encode($answer);
                        } else {
                            /* Update by DB type */
                            if(isset($tableSchemaColumns[$sFieldname]->dbType) && in_array($tableSchemaColumns[$sFieldname]->dbType,array('varchar(5)')) ) {
                                $answer = viewHelper::purified($answer);
                            }
                        }
                    }
                    if(empty($fname[1])) {
                        $ResultTableHTML .= "\t<tr class='printanswersquestion printanswersquestionhead'><td>{$questionHtml}</td><td class='printanswersanswertext'>".$answer."</td></tr>\n";
                        $ResultTableText .="* {$questionText} \t\t\t : {$fname[2]}\n";
                    } else {
                        $ResultTableHTML .= "\t<tr class='printanswersquestion'><td>{$subQuestionHtml}</td><td class='printanswersanswertext'>".$answer."</td></tr>\n";
                        $ResultTableText .="- \t {$subQuestionText} \t\t : {$fname[2]}\n";
                    }
                }
            }

        }
        $ResultTableHTML .= "</table>\n";
        $ResultTableText .= "\n\n";
        if(!$bIsHTML) {
            return $ResultTableText;
        }
        return $ResultTableHTML;
    }

    private function translate($string) {
        return parent::gT($string);
    }
}
